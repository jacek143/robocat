#!/usr/bin/env python
from camera import Camera
import cv2
import numpy as np
import yaml

class ImageFeatures():
    def __init__(self,sum,median,std):
        self.sum = sum
        self.median = median
        self.std = std

class ImageProcessor():
    def __init__(self):
        with open('config.yaml', 'r') as f:
            self._config = yaml.load(f)['image_processor']
        self._camera = Camera()
        self._img = self._camera.get_image()
        rows = self._img.shape[0]
        columns = self._img.shape[1]
        self._COL_WEIGHTS = 2*(np.tile(np.arange(columns,dtype=float),(rows,1))/(columns-1)-0.5)
        self._ROW_WEIGHTS = np.transpose(2*(np.tile(np.arange(rows,dtype=float),(columns,1))/(columns-1)-0.5))
        self._NUMBER_OF_PIXELS = rows*columns

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def close(self):
        self._camera.close()

    def get_features(self):
        self._img = self._camera.get_image()
        self._preprocess()
        return self._compute()

    def _preprocess(self):
        self._img = cv2.adaptiveThreshold(
            self._img,255,
            cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
            cv2.THRESH_BINARY_INV,
            self._config['block_size'],
            self._config['c'])

    def _compute(self):
        mask = self._img > 0
        sum = np.sum(mask,dtype=float)/self._NUMBER_OF_PIXELS
        col_masked = self._COL_WEIGHTS[mask]
        row_masked = self._ROW_WEIGHTS[mask]
        median = (np.median(row_masked),np.median(col_masked))
        std = (np.std(row_masked),np.std(col_masked))
        return ImageFeatures(sum,median,std)

    def show_processed_image(self):
        cv2.imshow("thresholded", self._img)
        cv2.waitKey(1)


if __name__ == "__main__":
    try:
        with ImageProcessor() as image_processor:
            while True:
                features = image_processor.get_features()
                print "sum: ", features.sum
                print "median: ", features.median
                print "std: ", features.std
                image_processor.show_processed_image()
    except KeyboardInterrupt:
        pass
