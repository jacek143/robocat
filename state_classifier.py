#!/usr/bin/env python
from enum import Enum
from image_processor import *

class State(Enum):
    GO_STRAIGHT = 1
    TURN_LEFT = 2
    TURN_RIGHT = 3
    SHARP_TURN_LEFT = 4
    SHARP_TURN_RIGHT = 5
    NO_LINE = 6

class StateClassifier:

    def __init__(self):
        self.processor = ImageProcessor()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def close(self):
        self.processor.close()

    def classify(self):
        features = self.processor.get_features()
        if features.sum < 0.1:
            return State.NO_LINE
        if features.std[1] < 0.2:
            return State.GO_STRAIGHT
        elif features.std[1] < 0.3:
            if features.median[1] > 0:
                return State.TURN_RIGHT
            else:
                return State.TURN_LEFT
        else:
            if features.median[1] > 0:
                return State.SHARP_TURN_RIGHT
            else:
                return State.SHARP_TURN_LEFT

if __name__ == "__main__":
    try:
        with StateClassifier() as classifier:
            while True:
                print classifier.classify()
    except KeyboardInterrupt:
        pass
