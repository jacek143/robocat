#!/usr/bin/env python
import RPi.GPIO as GPIO
import yaml
from time import sleep


class Motors:

    def __init__(self):
        with open('config.yaml', 'r') as f:
            config = yaml.load(f)['motors']
            if config['mode']=="BCM":
                GPIO.setmode(GPIO.BCM)
            else:
                GPIO.setmode(GPIO.BOARD)
            frequency = config['frequency']
            leftPwmPin = config['pins']['left']['pwm']
            self._leftIn1Pin = config['pins']['left']['in1']
            self._leftIn2Pin = config['pins']['left']['in2']
            rightPwmPin = config['pins']['right']['pwm']
            self._rightIn1Pin = config['pins']['right']['in1']
            self._rightIn2Pin = config['pins']['right']['in2']
            GPIO.setup(leftPwmPin, GPIO.OUT)
            GPIO.setup(self._leftIn1Pin, GPIO.OUT)
            GPIO.setup(self._leftIn2Pin, GPIO.OUT)
            GPIO.setup(rightPwmPin, GPIO.OUT)
            GPIO.setup(self._rightIn1Pin, GPIO.OUT)
            GPIO.setup(self._rightIn2Pin, GPIO.OUT)
            self._leftMotor = GPIO.PWM(leftPwmPin, frequency)
            self._rightMotor = GPIO.PWM(rightPwmPin, frequency)
            self._leftMotor.start(0)
            self._rightMotor.start(0)
        
    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def close(self):
        GPIO.cleanup()

    def set(self, left, right):
        if abs(left) > 100:
            msg = "Valid range for 'left' argument is [-100,100]"
            raise ValueError(msg)
        if abs(right) > 100:
            msg = "Valid range for 'right' argument is [-100,100]"
            raise ValueError(msg)
        left = int(left)
        right = int(right)
        isLeftPositive = left > 0
        GPIO.output(self._leftIn1Pin, isLeftPositive)
        GPIO.output(self._leftIn2Pin, not isLeftPositive)
        isRightPositive = right > 0
        GPIO.output(self._rightIn1Pin, isRightPositive)
        GPIO.output(self._rightIn2Pin, not isRightPositive)
        self._leftMotor.ChangeDutyCycle(abs(left))
        self._rightMotor.ChangeDutyCycle(abs(right))

if __name__ == "__main__":
    with Motors() as motors:
        motors.set(-10,10)
        sleep(2)
        motors.set(10,-10)
        sleep(2)
        motors.set(0,0)
