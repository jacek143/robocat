#!/usr/bin/env python
from camera import Camera
from motors import Motors
import cv2
import sys
import numpy as np
import yaml
import math


class LineFollower():
    def __init__(self):
        with open('config.yaml', 'r') as f:
            self._config = yaml.load(f)['line_follower']
        self._motors = Motors()
        self._camera = Camera()
        self._get_image()
        rows = self._img.shape[0]
        columns = self._img.shape[1]
        self._WEIGHTS = 2*(np.tile(np.arange(columns,dtype=float),(rows,1))/(columns-1)-0.5)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def close(self):
        self._camera.close()
        self._motors.close()

    def update(self):
        self._get_image()
        self._preprocess()
        self._compute()
        self._react()
        if self._config['debug']:
            cv2.waitKey(1)

    def _get_image(self):
        self._img = self._camera.get_image()
        if self._config['debug']:
            cv2.imshow("camera", self._img)

    def _preprocess(self):
        self._img = cv2.adaptiveThreshold(
            self._img,255,
            cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
            cv2.THRESH_BINARY_INV,
            self._config['preprocess']['block_size'],
            self._config['preprocess']['c'])
        if self._config['debug']:
            cv2.imshow("preprocess", self._img)

    def _compute(self):
        self._sum = np.sum(self._img>0)
        masked_weights = self._WEIGHTS[self._img>0]
        self._median = np.median(masked_weights)
        self._std = np.std(masked_weights)
        if self._config['debug']:
            print "sum %d, median: %f, std: %f" % (self._sum,self._median,self._std)

    def _react(self):
        if self._sum < self._config['react']['sum']:
            # sharp turn
            if self._median < 0:
                left = -self._config['react']['speed']
                right = self._config['react']['speed']
            else:
                left = self._config['react']['speed']
                right = -self._config['react']['speed']
        else:
            # other situations
            correction = self._median*self._config['react']['k']
            left = self._config['react']['speed'] + correction
            right = self._config['react']['speed'] - correction
        if self._config['debug']:
            print "left: %f, right: %f" % (left,right)
        else:
            self._motors.set(left,right)


if __name__ == "__main__":
    try:
        with LineFollower() as line_follower:
            while True:
                line_follower.update()
    except KeyboardInterrupt:
        pass
