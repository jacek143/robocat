#!/usr/bin/env python
import cv2
from picamera.array import PiYUVArray
from picamera import PiCamera
import time
import numpy as np
import os


class Camera:

    def __init__(self, resolution=(192, 128)):
        self._camera = PiCamera()
        time.sleep(2)
        self._camera.rotation = 180
        self._camera.resolution = resolution
        self._rawCapture = PiYUVArray(self._camera, size=resolution)
        self._stream = self._camera.capture_continuous(
            self._rawCapture, format='yuv', use_video_port=True)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def close(self):
        self._camera.close()

    def get_image(self):
        image = self._stream.next().array[:, :, 0]
        self._rawCapture.truncate(0)
        return image


if __name__ == "__main__":
    try:
        with Camera() as camera:
            print "Press whatever to capture image."
            while True:
                img = camera.get_image()
                cv2.imshow("Frame", img)
                if cv2.waitKey(1) != -1:
                    file_name = 'img/capture' + str(len(os.listdir('img')))
                    np.save(file_name, img)
                    print 'Image caputured as ' + file_name
    except KeyboardInterrupt:
        pass
